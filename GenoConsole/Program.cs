﻿using GenoAppBase;
using GenoAppBase.functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GenoConsole
{
    class Program
    {
        private static GenoApp ga;
        private static EventWaitHandle _waitHandle = new AutoResetEvent(false);

        static void Main(string[] args)
        {
          Console.WriteLine("start!");
        //    _waitHandle.Set();
            new Thread(new ThreadStart(run1)).Start();
            new Thread(new ThreadStart(run2)).Start();
            new Thread(new ThreadStart(run3)).Start();
            new Thread(new ThreadStart(run4)).Start();
            new Thread(new ThreadStart(run5)).Start();
            new Thread(new ThreadStart(run6)).Start();

            bool opt = true;
            while (opt)
            {
                switch (Console.ReadLine())
                {
                    case "stop":
                        ga.setStop(true);
                        break;
                    case "exit":
                        opt = false;
                        break;
                }
            }
        }

        private static readonly string PopuSize = "50";
        private static readonly string MutationRate = "25";
        private static readonly string CrossoverRate = "100";
        private static readonly string HybridRate = "100";
        private static readonly string CountStopper = "200000000";
        private static readonly string TimeStopper = "60";
        private static readonly string FunctionSize = "100";
        private static readonly string HybridAccuracy = "1";



        static void run1()
        {
     //       _waitHandle.WaitOne();
            ga = new GenoApp();
            ga.setPopulationSize(PopuSize);
            ga.setMutationRate(MutationRate);
            ga.setCrossoverRate(CrossoverRate);
            ga.setHybridRate(HybridRate);
            ga.setCountStopper(CountStopper);
            ga.setTimeStopper(TimeStopper);    // in minuts
            ga.setFunction(EFunction.SCHWEFEL, FunctionSize);
            ga.hybridEnabled(false);
            ga.setAccuracy(HybridAccuracy);
            Console.WriteLine("A genetic Algorithm is running in the metod run1()...");
            ga.run();
            Console.WriteLine("run1 has finished, wakes up run2");
            _waitHandle.Set();
        }



        static void run2()
        {
            Console.WriteLine("run2 is waiting for others runs.");
            _waitHandle.WaitOne();
            Console.WriteLine("run2 wakes up");
            ga = new GenoApp();
            ga.setPopulationSize(PopuSize);
            ga.setMutationRate(MutationRate);
            ga.setCrossoverRate(CrossoverRate);
            ga.setHybridRate(HybridRate);
            ga.setCountStopper(CountStopper);
            ga.setTimeStopper(TimeStopper);    // in minuts
            ga.setFunction(EFunction.SCHWEFEL, FunctionSize);
            ga.hybridEnabled(true);
            ga.setAccuracy(HybridAccuracy);
            Console.WriteLine("A genetic Algorithm is running in the metod run2()...");
            ga.run();
            Console.WriteLine("run2 has finished");
            _waitHandle.Set();
        }

        static void run3()
        {
            Console.WriteLine("run3 is waiting for others runs.");
            _waitHandle.WaitOne();
            Console.WriteLine("run3 wakes up");
            ga = new GenoApp();
            ga.setPopulationSize(PopuSize);
            ga.setMutationRate(MutationRate);
            ga.setCrossoverRate(CrossoverRate);
            ga.setHybridRate(HybridRate);
            ga.setCountStopper(CountStopper);
            ga.setTimeStopper(TimeStopper);    // in minuts
            ga.setFunction(EFunction.RASTRIGIN, FunctionSize);
            ga.hybridEnabled(false);
            ga.setAccuracy(HybridAccuracy);
            Console.WriteLine("A genetic Algorithm is running in the metod run3()...");
            ga.run();
            Console.WriteLine("run3 has finished");
            _waitHandle.Set();
        }

        static void run4()
        {
            Console.WriteLine("run4 is waiting for others runs.");
            _waitHandle.WaitOne();
            Console.WriteLine("run4 wakes up");
            ga = new GenoApp();
            ga.setPopulationSize(PopuSize);
            ga.setMutationRate(MutationRate);
            ga.setCrossoverRate(CrossoverRate);
            ga.setHybridRate(HybridRate);
            ga.setCountStopper(CountStopper);
            ga.setTimeStopper(TimeStopper);    // in minuts
            ga.setFunction(EFunction.RASTRIGIN, FunctionSize);
            ga.hybridEnabled(true);
            ga.setAccuracy(HybridAccuracy);
            Console.WriteLine("A genetic Algorithm is running in the metod run4()...");
            ga.run();
            Console.WriteLine("run4 has finished");
            _waitHandle.Set();
        }

        static void run5()
        {
            Console.WriteLine("run5 is waiting for others runs.");
            _waitHandle.WaitOne();
            Console.WriteLine("run5 wakes up");
            ga = new GenoApp();
            ga.setPopulationSize(PopuSize);
            ga.setMutationRate(MutationRate);
            ga.setCrossoverRate(CrossoverRate);
            ga.setHybridRate(HybridRate);
            ga.setCountStopper(CountStopper);
            ga.setTimeStopper(TimeStopper);    // in minuts
            ga.setFunction(EFunction.LEVI, FunctionSize);
            ga.hybridEnabled(false);
            ga.setAccuracy(HybridAccuracy);
            Console.WriteLine("A genetic Algorithm is running in the metod run5()...");
            ga.run();
            Console.WriteLine("run5 has finished");
            _waitHandle.Set();
        }

        static void run6()
        {
            Console.WriteLine("run6 is waiting for others runs.");
            _waitHandle.WaitOne();
            Console.WriteLine("run6 wakes up");
            ga = new GenoApp();
            ga.setPopulationSize(PopuSize);
            ga.setMutationRate(MutationRate);
            ga.setCrossoverRate(CrossoverRate);
            ga.setHybridRate(HybridRate);
            ga.setCountStopper(CountStopper);
            ga.setTimeStopper(TimeStopper);    // in minuts
            ga.setFunction(EFunction.LEVI, FunctionSize);
            ga.hybridEnabled(true);
            ga.setAccuracy(HybridAccuracy);
            Console.WriteLine("A genetic Algorithm is running in the metod run6()...");
            ga.run();
            Console.WriteLine("run6 has finished");
            _waitHandle.Set();
        }
    }
}
