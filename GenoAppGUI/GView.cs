﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenoAppGUI
{
    public partial class GView : UserControl
    {
        public GView()
        {
            InitializeComponent();
            gAlgorithm1.Visible = true;
            gTable1.Visible = false;
            gPanel1.Visible = false;
        }

        private void gSelectButton_Click(object sender, EventArgs e)
        {
            gTable1.Visible = true;
            gPanel1.Visible = false;
      //      gMain1.Visible = false;
            gTable1.loadData();
        }

        private void gDrawButton_Click(object sender, EventArgs e)
        {
            gTable1.Visible = false;
      //      gMain1.Visible = false;
            gPanel1.Visible = true;
            List<string> expFileNames = gTable1.getNames();
            gPanel1.draw(expFileNames);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            gAlgorithm1.Visible = true;
            gTable1.Visible = false;
            gPanel1.Visible = false;
        }

        private void gResultsButton_Click(object sender, EventArgs e)
        {
            gTable1.Visible = true;
            gPanel1.Visible = false;
            gAlgorithm1.Visible = false;
            gTable1.loadData();
        }

        //refresh table
        private void button2_Click(object sender, EventArgs e)
        {
            gTable1.refresh();
        }

        /*
         private void gSelectButton_Click(object sender, EventArgs e)
        {
            gTable1.Visible = true;
            gPanel1.Visible = false;
      //      gMain1.Visible = false;
            gTable1.loadData();
        }

        private void gDrawButton_Click(object sender, EventArgs e)
        {
            gTable1.Visible = false;
      //      gMain1.Visible = false;
            gPanel1.Visible = true;
            List<string> expFileNames = gTable1.getNames();
            gPanel1.draw(expFileNames);
        }
        */
    }
}
