﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GenoAppBase.dataobject;
using ZedGraph;
using GenoAppBase.datalayer;

namespace GenoAppGUI
{
    public partial class GPanel : UserControl
    {
        public GPanel()
        {
            InitializeComponent();
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
        }

        private ZedGraphControl z1 = new ZedGraphControl();
        private IGenoExpDAO dataDAO = GenoDaoFactory.getInstance().getDao(DataType.FILE);
        private List<Conv> convs = new List<Conv>();
        private int sizePointsX = 0;   //is the size for x points. It can be changed by change zoom

        public void draw(List<string> expFileNames)
        {
            button1.Click += new EventHandler(button1_Click);
            textbox1.Click += new EventHandler(textbox1_Click);
            //next settings must be return to the initial state
            convs.Clear();
            sizePointsX = 0;
            minMaxValue = null;

            //load data and save it in convs list
            foreach (var name in expFileNames)
            {
                if(convs.FindIndex(s => s.Name.Equals(name)) < 0)
                {
                    ExperimentInfo infoTemp;
                    convs.Add(new Conv(dataDAO.getExperimentData(name), infoTemp = dataDAO.getExperimentByName(name), (bool)infoTemp.IsHybrid ? Color.Red : Color.Blue));
                }
            }
            //drawing
            drawAll();
        }

        private void drawAll()
        {
            panel1.Controls.Clear();
            z1.GraphPane.CurveList.Clear();
            settings();

            foreach(Conv c in convs)
            {

                double[] tempX = new double[getMinMaxLength(c.Data.X)];
                double[] tempY = new double[getMinMaxLength(c.Data.X)];

                if (radioButton1.Checked)
                {
                    //by time
                    Array.Copy(c.Data.X, tempX, tempX.Length);
                }
                else if (radioButton2.Checked)
                {
                    //by iterations
                    tempX = new double[getMinMaxLength(c.Data.Z)];
                    Array.Copy(c.Data.Z, tempX, tempX.Length);
                }
                
                Array.Copy(c.Data.Y, tempY, tempY.Length);

                string type = (bool)c.Info.IsHybrid ? "hybrid" : "classic";
                string label = c.Info.FunctionName + "_" + type;

                z1.GraphPane.AddCurve(label, tempX, tempY, c.Color, SymbolType.None);
            }

            z1.AxisChange();
            z1.Invalidate();
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            drawAll();
        }

        private int getMinMaxLength(double[] xpoints)
        {
            int minMaxLenght = 0;
            try
            {
                double currentMinMaxValue = getMinMaxValue();
                for (int i = 0; i < xpoints.Length; i++)
                {
                    if(xpoints[i] <= currentMinMaxValue)
                    {
                        minMaxLenght++;
                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return minMaxLenght;
        }

        private double? minMaxValue;
        private double getMinMaxValue()
        {
            try
            {
                if(minMaxValue == null)
                {
                    minMaxValue = convs.Min(s => s.Data.X.Max());
                }  
            }
            catch (Exception e)
            {
                throw new Exception("There are not any points to draw.");
            }
            return (double)minMaxValue;
        }

        private void settings()
        {
            z1.GraphPane.Title = "GA Convergence";
            z1.IsShowPointValues = true;
            this.SuspendLayout();
            // 
            // zedGraphControl1
            // 
            this.z1.Location = new System.Drawing.Point(0, 0);
            this.z1.Name = "zedGraphControl1";
            this.z1.Size = new System.Drawing.Size(panel1.Width, panel1.Height);
            this.z1.TabIndex = 0;

            //
            // build zoom buttons and textbox
            //
            buildZoomTextBox(z1.Size);

            // add radiobuttons to drawer
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);

            // 
            // Form1
            // 
            //     this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            //  this.ClientSize = new System.Drawing.Size(1200, 600);
            panel1.Controls.Add(z1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            //       this.Load += new System.EventHandler(Form1_Load);

        }


        private Button button1 = new Button(), button2 = new Button();
        private TextBox textbox1 = new TextBox();
        private int maxH;
        private const string TEXT_MAX_VALUE = "Max value";

        private void buildZoomTextBox(Size size)
        {
            int sizeH = 80;
            int sizeV = 30;
            int bottomMargin = 40;
            int rightMargin = 60;

            this.textbox1.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
            this.textbox1.BackColor = SystemColors.ControlLightLight;
            this.textbox1.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            this.textbox1.Location = new Point(size.Width - sizeH - rightMargin, size.Height - bottomMargin);
            this.textbox1.Name = "button1";
            this.textbox1.Size = new Size(sizeH, sizeH);
            this.textbox1.TabIndex = 0;
            textbox1.Text = TEXT_MAX_VALUE;
            buildZoomOkButton(size);
            z1.Controls.Add(textbox1);
        }
        private void buildZoomOkButton(Size size)
        {
            int sizeH = 50;
            int sizeV = 30;
            int rightMargin = 10;
   
            // 
            // button1
            // 
            this.button1.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
            this.button1.BackColor = SystemColors.ControlLightLight;
            this.button1.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new Point(size.Width - sizeH - rightMargin, size.Height - 42);
            this.button1.Name = "button1";
            this.button1.Size = new Size(sizeH, sizeV);
            this.button1.TabIndex = 0;
            this.button1.Text = "ok";
            this.button1.UseVisualStyleBackColor = false;

            z1.Controls.Add(button1);
        }

        //zoom
        private void button1_Click(object sender, System.EventArgs e)
        {
            double temp;
            if(double.TryParse(textbox1.Text, out temp))
            {
                minMaxValue = temp;
                drawAll();
            }
        }

        private void textbox1_Click(object sender, System.EventArgs e)
        {
            if (textbox1.Text.Equals(TEXT_MAX_VALUE))
            {
                textbox1.Text = "";
            }
        }

        //draw by iterations
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            drawAll();
        }

        //draw by time
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            drawAll();
        }

        private class Conv
        {
            public ExperimentData Data { get; }
            public string Name { get; }
            public Color Color { get; }
            public ExperimentInfo Info { get; }

            public Conv(ExperimentData data, ExperimentInfo info, Color color)
            {
                Data = data;
                Color = color;
                Info = info;
                Name = info.Id;
            }

            public override bool Equals(object obj)
            {
                if (obj == null || GetType() != obj.GetType())
                    return false;

                Conv c = (Conv)obj;

                return Name == c.Name;
            } 
        }
    }
}
