﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GenoAppBase;
using GenoAppBase.functions;
using GenoAppBase.datalayer;
using GenoAppBase.datalayer.impl;
using GenoAppBase.dataobject;
using System.Threading;
using GenoAppBase.util;

namespace GenoAppGUI
{
    public partial class GAlgorithm : UserControl
    {
        public GAlgorithm()
        {
            InitializeComponent();
            comboBox1.DataSource = Enum.GetValues(typeof(EFunction));
            // populate GenoApp GUI
            populateGenoAppGUI();
        }

        private BindingList<GenoApp> genoapps = new BindingList<GenoApp>();
        private IGenoExpDAO dao = GenoDaoFactory.getInstance().getDao(DataType.FILE);
        private ExperimentInfo defaultInfo = null;
        
        // add new experiment
        private void button1_Click(object sender, EventArgs e)
        {
            if(index != null)
            {
                //just update
                genoapps[(int)index] = getGenoAppFromGUI();
                log("item from list has been updated");
                index = null;
            }
            else
            {
                genoapps.Add(getGenoAppFromGUI());
            }
            
            listBox1.DataSource = genoapps;
        }

        // save default
        private void button3_Click(object sender, EventArgs e)
        {
            dao.saveDefaultExperimentInfo(getGenoAppFromGUI().getExperimentInfo());
        }

        // load default
        private void button4_Click(object sender, EventArgs e)
        {
            populateGenoAppGUI();
        }

        // run all genetic algorithms in separate thread 
        private void button2_Click(object sender, EventArgs e)
        {
            if(genoapps.Count == 0)
            {
                log("nothing to run.");
                return;
            }
            Thread t = new Thread(new ThreadStart(run));
            t.Start();

        }

        // runner
        private void run()
        {

            foreach (var app in genoapps)
            {
                this.Invoke((MethodInvoker)delegate {
                    log(app.ToString() + " is running...");
                    //disable run button while genoapp is not finished
                    button2.Enabled = false;
                });
                app.run();
            }

            this.Invoke((MethodInvoker)delegate {
                log("all genetic algorithms have finished..");
                //enable run button when genoapp is finished
                button2.Enabled = true;
            });
            
        }

        // finishing all runnig genoapps
        private void button5_Click(object sender, EventArgs e)
        {
            if(genoapps.Count == 0)
            {
                log("no genoapps are running");
                return;
            }

            log("trying to stop all genoapps...");
            foreach (var app in genoapps)
            {
                app.setStop(true);
            }
        }


        private void log(string message)
        {
            textBox1.AppendText("\r\n" + message);
            Log.info(message);
        }

        // helper
        private GenoApp getGenoAppFromGUI()
        {
            GenoApp ga = new GenoApp();
            EFunction function;
            Enum.TryParse<EFunction>(comboBox1.SelectedValue.ToString(), out function);
            ga.setFunction(function, textBox9.Text);
            ga.setPopulationSize(textBox2.Text);
            ga.setMutationRate(textBox3.Text);
            ga.setCrossoverRate(textBox4.Text);
            ga.setCountStopper(textBox5.Text);
            ga.setTimeStopper(textBox6.Text);    // in minuts
            ga.hybridEnabled(checkBox1.Checked);
            ga.setHybridRate(textBox7.Text);
            ga.setAccuracy(textBox8.Text);
            return ga;
        }

        private void populateFromInfo(ExperimentInfo info)
        {
            textBox2.Text = info.PopulationSize.ToString();
            textBox3.Text = info.MutationRate.ToString();
            textBox4.Text = info.CrossoverRate.ToString();
            textBox5.Text = info.CountStopper.ToString();
            textBox6.Text = info.TimeStopper.ToString();
            textBox7.Text = info.HybridRate.ToString();
            textBox8.Text = info.Accuracy.ToString();
            textBox9.Text = info.FunctionSize.ToString();
            checkBox1.Checked = (bool)info.IsHybrid;
            comboBox1.SelectedItem = info.FunctionName;
        }
        private void populateGenoAppGUI()
        {
            if ((defaultInfo = dao.getDefaultExperimentInfo()) != null)
            {
                populateFromInfo(defaultInfo);
            }
            else
            {
                textBox2.Text = PopuSize;
                textBox3.Text = MutationRate;
                textBox4.Text = CrossoverRate;
                textBox5.Text = CountStopper;
                textBox6.Text = TimeStopper;
                textBox7.Text = HybridRate;
                textBox8.Text = HybridAccuracy;
                textBox9.Text = FunctionSize;
            }
        }

        //
        // defaults
        //
        private static readonly string PopuSize = "50";
        private static readonly string MutationRate = "25";
        private static readonly string CrossoverRate = "100";
        private static readonly string HybridRate = "100";
        private static readonly string CountStopper = "200000000";
        private static readonly string TimeStopper = "60";
        private static readonly string FunctionSize = "100";
        private static readonly string HybridAccuracy = "1";

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Delete)
            {
                genoapps.Remove((GenoApp)listBox1.SelectedItem);
            }
        }

        //doble click en item
        int? index = null;
        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            index = this.listBox1.IndexFromPoint(e.Location);
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                populateFromInfo(genoapps[(int)index].getExperimentInfo());
            }
        }

        //!hybrid
        private void button6_Click(object sender, EventArgs e)
        {
            if (genoapps.Count == 0)
            {
                log("no genoapps are running");
                return;
            }

            log("trying to stop hybrid...");
            foreach (var app in genoapps)
            {
                app.removeHybrid(true);
            }
        }

        //hybrid
        private void button7_Click(object sender, EventArgs e)
        {
            if (genoapps.Count == 0)
            {
                log("no genoapps are running");
                return;
            }

            log("trying to start all hybrid...");
            foreach (var app in genoapps)
            {
                app.removeHybrid(false);
            }
        }
    }
}
