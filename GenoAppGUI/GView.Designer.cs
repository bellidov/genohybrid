﻿namespace GenoAppGUI
{
    partial class GView
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.gDrawButton = new System.Windows.Forms.Button();
            this.gResultsButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.gAlgorithm1 = new GenoAppGUI.GAlgorithm();
            this.gPanel1 = new GenoAppGUI.GPanel();
            this.gTable1 = new GenoAppGUI.GTable();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.gAlgorithm1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.gPanel1);
            this.panel1.Controls.Add(this.gTable1);
            this.panel1.Controls.Add(this.gDrawButton);
            this.panel1.Controls.Add(this.gResultsButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MinimumSize = new System.Drawing.Size(800, 500);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 500);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(0, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 33);
            this.button1.TabIndex = 4;
            this.button1.Text = "Algorithm";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gDrawButton
            // 
            this.gDrawButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gDrawButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.gDrawButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gDrawButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gDrawButton.Location = new System.Drawing.Point(688, 4);
            this.gDrawButton.Name = "gDrawButton";
            this.gDrawButton.Size = new System.Drawing.Size(94, 33);
            this.gDrawButton.TabIndex = 1;
            this.gDrawButton.Text = "Draw";
            this.gDrawButton.UseVisualStyleBackColor = true;
            this.gDrawButton.Click += new System.EventHandler(this.gDrawButton_Click);
            // 
            // gResultsButton
            // 
            this.gResultsButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.gResultsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gResultsButton.Font = new System.Drawing.Font("Microsoft Tai Le", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gResultsButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gResultsButton.Location = new System.Drawing.Point(113, 5);
            this.gResultsButton.Name = "gResultsButton";
            this.gResultsButton.Size = new System.Drawing.Size(87, 33);
            this.gResultsButton.TabIndex = 0;
            this.gResultsButton.Text = "Results";
            this.gResultsButton.UseVisualStyleBackColor = true;
            this.gResultsButton.Click += new System.EventHandler(this.gResultsButton_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(215, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 33);
            this.button2.TabIndex = 6;
            this.button2.Text = "Refresh";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // gAlgorithm1
            // 
            this.gAlgorithm1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gAlgorithm1.BackColor = System.Drawing.SystemColors.Info;
            this.gAlgorithm1.Location = new System.Drawing.Point(0, 43);
            this.gAlgorithm1.Name = "gAlgorithm1";
            this.gAlgorithm1.Size = new System.Drawing.Size(800, 458);
            this.gAlgorithm1.TabIndex = 5;
            // 
            // gPanel1
            // 
            this.gPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gPanel1.Location = new System.Drawing.Point(0, 42);
            this.gPanel1.Name = "gPanel1";
            this.gPanel1.Size = new System.Drawing.Size(800, 458);
            this.gPanel1.TabIndex = 3;
            // 
            // gTable1
            // 
            this.gTable1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gTable1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gTable1.Location = new System.Drawing.Point(0, 42);
            this.gTable1.Name = "gTable1";
            this.gTable1.Size = new System.Drawing.Size(800, 458);
            this.gTable1.TabIndex = 2;
            // 
            // GView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.panel1);
            this.Name = "GView";
            this.Size = new System.Drawing.Size(800, 500);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button gDrawButton;
        private System.Windows.Forms.Button gResultsButton;
        private GTable gTable1;
        private GPanel gPanel1;
        private System.Windows.Forms.Button button1;
        private GAlgorithm gAlgorithm1;
        private System.Windows.Forms.Button button2;
    }
}
