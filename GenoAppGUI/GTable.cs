﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GenoAppBase.dataobject;
using GenoAppBase.datalayer;

namespace GenoAppGUI
{
    public partial class GTable : UserControl
    {
        public GTable()
        {
            InitializeComponent();
        }

        private List<ExperimentInfo> info = new List<ExperimentInfo>();
        private IGenoExpDAO dao = GenoDaoFactory.getInstance().getDao(DataType.FILE); 

        public void loadData()
        {
            DataTable table = new DataTable();
            table.Columns.Add("file");
            table.Columns.Add("Function");
            table.Columns.Add("size");
            table.Columns.Add("hybrid");
            table.Columns.Add("hybrid rate");
            table.Columns.Add("time (sec.)");
            table.Columns.Add("iterations");
            table.Columns.Add("population");
            table.Columns.Add("mutation");
            table.Columns.Add("crossover");
            

            for (int i = getInfo().Count - 1; i >= 0; i--)
            {
                table.Rows.Add(getInfo()[i].Id, 
                               getInfo()[i].FunctionName, 
                               getInfo()[i].FunctionSize, 
                               getInfo()[i].IsHybrid,
                               getInfo()[i].HybridRate,
                               getInfo()[i].getData().Time,
                               getInfo()[i].getData().Iterations,
                               getInfo()[i].PopulationSize,
                               getInfo()[i].MutationRate, 
                               getInfo()[i].CrossoverRate 
                               );
            }

            gDataGrid.DataSource = table;
        }

        private List<ExperimentInfo> getInfo()
        {
            if(info.Count == 0)
            {
                info = dao.getExperiments();
            }
            return info;
        }

        public void refresh()
        {
            info.Clear();
            loadData();
        }

        public List<string> getNames()
        {
            List<string> expFileNames = new List<string>();

            foreach (DataGridViewRow row in gDataGrid.SelectedRows)
            {
                expFileNames.Add(row.Cells[0].Value.ToString());
            }

            return expFileNames;
        }

    }
}
