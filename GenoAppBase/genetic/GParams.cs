﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.genetic
{
    class GParams
    {
        public const string EXPERIMENT = "Experiment file name";
        public const string POPULATION = "Population size";
        public const string CROSSOVER = "Crossover rate";
        public const string MUTATION = "Mutation rate";
        public const string COUNTSTOPPER = "Stop counter";
        public const string FUNCTION = "Function";
        public const string FUNCTIONSIZE = "Function size";
        public const string HYBRID = "Hybrid mode";
        public const string ACCURACY = "Strongin accuracy";
        public const string MINARG = "Min argument";
        public const string MINVALUE = "Min value";
        public const string GENERATIONS = "Number of iterations";
        // time is not used for the moment
        public const string TIME = "Spend time";
        public const string HYBRIDRATE = "Hybrid rate";



        public static List<string> exp()
        {
            List<string> list = new List<string>();
            list.Add(EXPERIMENT);
            list.AddRange(genoapp());

            //this is not experiment info, this is experiment data (results)
            list.Add(MINARG);
            list.Add(MINVALUE);
            list.Add(GENERATIONS);

            return list;
        }

        public static List<string> genoapp()
        {
            List<string> list = new List<string>();
            list.Add(POPULATION);
            list.Add(CROSSOVER);
            list.Add(MUTATION);
            list.Add(COUNTSTOPPER);
            list.Add(FUNCTION);
            list.Add(FUNCTIONSIZE);
            list.Add(HYBRID);
            list.Add(ACCURACY);
            list.Add(HYBRIDRATE);

            return list;
        }
    }
}
