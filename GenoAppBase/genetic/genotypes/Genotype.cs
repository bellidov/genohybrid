﻿using GenoAppBase.functions;

namespace GenoAppBase.genetic.genotypes
{
    class Genotype : IGenotype
    {
        private double[] genotype;
        private double rang;

        public Genotype(double[] genotype, IFunction function)
        {
            this.genotype = (double[])(genotype.Clone());
            rang = function.getValue(genotype);
        }

        public Genotype(double[] genotype, double rang)
        {
            this.genotype = (double[])(genotype.Clone());
            this.rang = rang;
        }

        public double getRang()
        {
            return rang;
        }

        public double[] getGenotype()
        {
            return genotype;
        }

        public override string ToString()
        {
            return string.Format("{1} : ({0})", string.Join(", ", genotype), rang);
        }
    }
}
