﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;
using System.IO;
using GenoAppBase.util;

namespace GenoAppBase.genetic.operators.impl
{
    class WriterLogArg : IGeneticOperator
    {
        private string fileName;

        public WriterLogArg(string fileName)
        {
            this.fileName = fileName;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction function)
        {
            using (StreamWriter writer = new StreamWriter(fileName, true, Encoding.Default))
            {
                writer.WriteLine(Util.popuToString(population));
            }
            return population;
        }
    }
}
