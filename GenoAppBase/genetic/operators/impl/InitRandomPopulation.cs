﻿using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.genetic.operators.impl
{
    public class InitRandomPopulation : IGeneticOperator
    {
        private int N;

        public InitRandomPopulation(int N)
        {
            this.N = N;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction function)
        {
            if (population.Count != 0)
            {
                return population;
            }

            double[] a = function.getDomain().getMin();
            double[] b = function.getDomain().getMax();

            Random r = new Random();
            int Fdim = function.getDim();
            double[] genotemp = new double[Fdim];

            for (int i = 0; i < N; i++)
            {
                int j = 0;
                while (j < Fdim)
                {
                    genotemp[j] = r.NextDouble() * (b[j] - a[j]) + a[j];  // beru sluchainie DOPUSTIMIE parametri argumenta
                    j++;
                }
                population.Add(new Genotype(genotemp, function));  // dobavliayu v populiatsiu
            }

            return population;
        }
    }
}
