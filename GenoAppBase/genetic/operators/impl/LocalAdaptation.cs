﻿using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;
using GenoAppBase.strongin;
using GenoAppBase.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.genetic.operators.impl
{
    public class LocalAdaptation : IGeneticOperator
    {
        private IOptimizator finder;
        private int dim = 0;
        private double accuracy;
        private int rate;

        public LocalAdaptation(double accuracy, int rate)
        {
            this.accuracy = accuracy;
            this.rate = rate;
        }

        private int getRandomIndex(int min, int max, HashSet<int> exclude)
        {
            var range = Enumerable.Range(min, max).Where(i => !exclude.Contains(i));
            int rangeIndex = new Random().Next(min, max - exclude.Count);
            int index = range.ElementAt(rangeIndex);
            exclude.Add(index);
            return index;
        }

        private Random r = new Random();
        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
                int p = r.Next(0, 100);
                if(p > 30)
                {
                    return population;
                }
            List<IGenotype> popu = new List<IGenotype>(); // to chto budet na vyxode

            int count = 0;
            if (dim == 0)
            {
                dim = f.getDim();
            }

            double[] a = f.getDomain().getMin();
            double[] b = f.getDomain().getMax();


            HashSet<int> exclude = new HashSet<int>();
            // cycle proxodit po kazhdoi osobi v populiatsii
            int N = population.Count;
            int max = rate *  N / 100;  // if rate == 100 => we use local adaptation for all genotypes in population
            while (count < max)
            {
                //get random index from population
                int ip = getRandomIndex(0, N, exclude);
                double[] argMin = (double[])population[ip].getGenotype().Clone(); // delayu kopiu tekushego massiva
                IGenotype genoMin; // budet dobavliatsa v populiatsiu
                double minRang = population[ip].getRang();

                // cycl proxodit po kazhdomu elementu tekushego genotypa
                for (int i = 0; i < dim; i++)
                {
           //         genoMin = new Genotype(argMin, f);

                    double[] argTemp = (double[])argMin.Clone(); // vremenno, dlia sravnenia
                    finder = new NewStrongin(a[i], b[i], argTemp, i, accuracy); // po ocheredi uluchaetsa kazhdiy element genotypa
                    argTemp[i] = finder.argMin(f);
               //     IGenotype genoTemp = new Genotype(argTemp, f);

                    // uslovie ostanova

                    if (finder.getMinFunction() <= minRang)
                    {
                        //esli bylo ulushenie, to soxraniayu ego
                        argMin = (double[])argTemp.Clone();
                        minRang = finder.getMinFunction();
                    }
                    else
                    {
                        //esli net ulushenia, to vyxozhu is tsikla chtoby pereiti k sleduyushei osobi
                        break;
                    }

                }

                genoMin = new Genotype(argMin, minRang);
                popu.Add(genoMin);
                count++;
            }

            population.AddRange(popu);
            return population;
        }
    }


    class LocalAdaptationTest : IGeneticOperator
    {
        private IOptimizator finder;
        private int dim = 0;
        double[] a, b;

        public LocalAdaptationTest(double accuracy, int rate)
        {
            //  this.accuracy = accuracy;
            //  this.rate = rate;
            this.a = new double[100];
            this.b = new double[100];

            for(int i = 0; i < 100; i++)
            {
                a[i] = -500;
                b[i] = 500;
            }
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            List<IGenotype> popu = new List<IGenotype>(); // to chto budet na vyxode

            int count = 0;
            if (dim == 0)
            {
                dim = f.getDim();
            }

            // cycle proxodit po kazhdoi osobi v populiatsii
            while (count < population.Count)
            {
                double[] argMin = (double[])population[count].getGenotype().Clone(); // delayu kopiu tekushego massiva
                IGenotype genoMin; // budet dobavliatsa v populiatsiu popu

                // cycl proxodit po kazhdomu elementu tekushego genotypa
                for (int i = 0; i < dim; i++)
                {
                    genoMin = new Genotype(argMin, f);

                    double[] argTemp = (double[])argMin.Clone(); // vremenno, dlia sravnenia
                    finder = new Strongin(a[i], b[i], argTemp, i, 1); // po ocheredi uluchaetsa kazhdiy element genotypa
                    argTemp[i] = finder.argMin(f);
                    IGenotype genoTemp = new Genotype(argTemp, f);

                    // uslovie ostanova
                    if (genoTemp.getRang() <= genoMin.getRang())
                    {
                        //esli bylo ulushenie, to soxraniayu ego
                        argMin = (double[])argTemp.Clone();
                    }
                    else
                    {
                        //esli net ulushenia, to vyxozhu is tsikla chtoby pereiti k sleduyushei osobi
                        break;
                    }

                }

                genoMin = new Genotype(argMin, f);
                popu.Add(genoMin);
                count++;
            }
            //fin de ciclo

            return popu;
        }
        /*      private IOptimizator finder;
              private int dim = 0;
              private double accuracy;
              private int rate;

              public LocalAdaptation(double accuracy, int rate)
              {
                  this.accuracy = accuracy;
                  this.rate = rate;
              }

              public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
              {
                  List<IGenotype> popu = new List<IGenotype>(); // to chto budet na vyxode

                  int count = 0;
                  if (dim == 0)
                  {
                      dim = f.getDim();
                  }

                  double[] a = f.getDomain().getMin();
                  double[] b = f.getDomain().getMax();

                  // cycle proxodit po kazhdoi osobi v populiatsii
                  int max = rate * population.Count / 100;
                  while (count < max)
                  {
                      double[] argMin = (double[])population[count].getGenotype().Clone(); // delayu kopiu tekushego massiva
                      IGenotype genoMin; // budet dobavliatsa v populiatsiu popu

                      // cycl proxodit po kazhdomu elementu tekushego genotypa
                      for (int i = 0; i < dim; i++)
                      {
                          genoMin = new Genotype(argMin, f);

                          double[] argTemp = (double[])argMin.Clone(); // vremenno, dlia sravnenia
                          finder = new Strongin(a[i], b[i], argTemp, i, accuracy); // po ocheredi uluchaetsa kazhdiy element genotypa
                          argTemp[i] = finder.argMin(f);
                          IGenotype genoTemp = new Genotype(argTemp, f);

                          // uslovie ostanova
                          if (genoTemp.getRang() <= genoMin.getRang())
                          {
                              //esli bylo ulushenie, to soxraniayu ego
                              argMin = (double[])argTemp.Clone();
                          }
                          else
                          {
                              //esli net ulushenia, to vyxozhu is tsikla chtoby pereiti k sleduyushei osobi
                              break;
                          }

                      }

                      genoMin = new Genotype(argMin, f);
                      popu.Add(genoMin);
                      count++;
                  }
                  //fin de ciclo

                  return popu;
              }*/
    }
}
