﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;

namespace GenoAppBase.genetic.operators.impl
{
    public class NothingToDo : IGeneticOperator
    {
        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction function)
        {
            return population;
        }
    }
}
