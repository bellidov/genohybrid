﻿using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GenoAppBase.genetic.operators.impl
{
    public class WriterLog : IGeneticOperator
    {
        private DateTime current, start;
        private DateTime? last = null;
        private string fileName;
        private GeneticAlgoritm ga;

        public WriterLog(String fileName, DateTime start, GeneticAlgoritm ga)
        {
            this.start = start;
            this.ga = ga;
          //  last = start;
            this.fileName = fileName;
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            current = DateTime.Now;
            
            if (last != null && current.Subtract((DateTime)last).TotalMilliseconds < 1000)
            {
                return population;
            }

            //    fileName = "pios.txt";
            using (StreamWriter writer = new StreamWriter(fileName, true, Encoding.Default))
            {
                double bestFitness = getBestFitness(population);
                double averageFitness = getFitnessMedium(population);

                double interval = current.Subtract(start).TotalMilliseconds;

                population.Sort(new Comp());
                writer.WriteLine("{0} {1} {2} {3} {4} {5}", bestFitness, averageFitness, interval, DateTime.Now.ToString("HH:mm:ss:fff"), ga.getGenerations(), population[0]);
                last = current;
            }
            return population;
        }

        // vychisliaet srednuyu prisposoblennost dlia kazhdoi populiatsii
        private double getFitnessMedium(IEnumerable<IGenotype> population)
        {
            return population.Average(genotype => genotype.getRang());
        }

        private double getBestFitness(IEnumerable<IGenotype> population)
        {
            
            return population.Min(genotype => genotype.getRang());
        }
    }
}
