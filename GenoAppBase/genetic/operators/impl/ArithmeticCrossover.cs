﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;

namespace GenoAppBase.genetic.operators.impl
{
    public class ArithmeticCrossover : IGeneticOperator
    {

        private int crossingRate;

        public ArithmeticCrossover(int crossingRate)
        {
            this.crossingRate = crossingRate;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction function)
        {
            int L = population[0].getGenotype().Length;
            int N = population.Count;
            int Fdim = function.getDim();
            double alpha;
            int max = (int)(this.crossingRate * N / 100); // maximalnoe kolichestvo brachnix par

            Random r = new Random();
            alpha = r.NextDouble(); // sluchainaya tochka dlia skreshivania

            for (int k = 0; k < max - 1; k = k + 2) // beru po 2 osobi i skreshivayu ix
            {
                int n = 0, m = 0; // indeksi dlia sluchainix roditelei
                n = new Random().Next(0, population.Count);
                m = new Random().Next(0, population.Count);

                double[] ancestor1 = new double[Fdim];
                double[] ancestor2 = new double[Fdim];

                double[] parent1 = population[n].getGenotype();
                double[] parent2 = population[m].getGenotype();

                for (int i = 0; i < L; i++)
                {
                    ancestor1[i] = alpha * parent1[i] + (1 - alpha) * parent2[i];
                    ancestor2[i] = (1 - alpha) * parent1[i] + alpha * parent2[i];
                }

                population.Add(new Genotype(ancestor1, function));
                population.Add(new Genotype(ancestor2, function));
            }
            return population;
        }
    }
}
