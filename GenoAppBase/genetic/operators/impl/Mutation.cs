﻿using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.genetic.operators.impl
{
    public class Mutation : IGeneticOperator
    {
        private int mutationRate;

        public Mutation(int mutationRate)
        {
            this.mutationRate = mutationRate;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            int L = population[0].getGenotype().Length; // dlina genotype
            int N = population.Count;
            int Fdim = f.getDim();

            double[] a = f.getDomain().getMin();
            double[] b = f.getDomain().getMax();

            Random r = new Random();
            int count = 0;
            int max = (int)(mutationRate * N / 100);

            while (count < max)
            {
                int ip = r.Next(0, N); // index dlia populiatsii
                int ig = r.Next(0, L); // index dlia genotype
                double value = r.NextDouble() * (b[ig] - a[ig]) + a[ig]; // sluchainoe razreshennoe znachenie dlia gena

                double[] genotemp = new double[Fdim];
                for (int i = 0; i < Fdim; i++)
                {
                    genotemp[i] = population[ip].getGenotype()[i]; // sozdayu mutirovannij gen
                }
                genotemp[ig] = value; // sobstvenno, mutatsia

                population.Add(new Genotype(genotemp, f));

                count++;
            }

            return population;
        }
    }
}
