﻿using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;
using System;
using System.Collections.Generic;


namespace GenoAppBase.genetic.operators
{
    public interface IGeneticOperator
    {
        List<IGenotype> getPopulation(List<IGenotype> population, IFunction function);
    }
}
