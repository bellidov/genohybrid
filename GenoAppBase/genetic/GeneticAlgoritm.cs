﻿using GenoAppBase.functions;
using GenoAppBase.genetic.genotypes;
using GenoAppBase.genetic.operators;
using GenoAppBase.genetic.operators.impl;
using GenoAppBase.strongin;
using GenoAppBase.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GenoAppBase.genetic
{
    public class GeneticAlgoritm
    {
        private int popuSize; // size of population
        private int stopCounter; // number of unchangeable generations to stop the app
        private int generations = 0; // number of iterations
        private List<IGenotype> population = new List<IGenotype>();
        private List<IGeneticOperator> operators = new List<IGeneticOperator>();
        private bool stop = false;  // is used to stop the GA from console/GUI
        private DateTime startTime;
        private int stopTimer;   // is used to stop the GA from console/GUI in minutes


        public GeneticAlgoritm(int stopCounter, int stopTimer, int popuSize, DateTime startTime)
        {
            this.stopCounter = stopCounter;
            this.popuSize = popuSize;
            this.startTime = startTime;
            this.stopTimer = stopTimer;
        }

        public void setStop(bool stop)
        {
            //we use this flag to enable user to stop the GA from GUI/console
            this.stop = stop;
            NewStrongin.stop = stop;
        }

        private bool timeOut()
        {
            // stopTimer < 1 means that timer was not setted, so whe always return true (no time out required)
            return stopTimer < 1 ? true : DateTime.Now.Subtract(startTime).TotalMinutes < stopTimer;
        }

        int i = 0;
        public double[] FindMinArg(IFunction function)
        {
            //set the init population
            population = operators[0].getPopulation(population, function);         

            int count = 0; // generation count
            double currentOptimum = population.Min(genotype => genotype.getRang());

            // while the number of generation without changes is less then stopCounter
            while (count < stopCounter && !stop && timeOut())
            {
                generations++;
                //run each genetic operator (except operator[0] == init population operator)
                for (int i = 1; i < operators.Count; i++)
                {
                    population = operators[i].getPopulation(population, function);
                }

                //stop condition
                double newOptimum = population.Min(genotype => genotype.getRang());
                if(newOptimum >= currentOptimum)
                {
                    count++;
                }
                else
                {
                    count = 0;
                    currentOptimum = newOptimum;
                }
            }
            return population[0].getGenotype();
        }

        public void setOperators(params IGeneticOperator[] geneticOperator)
        {
            foreach(var oper in geneticOperator)
            {
                operators.Add(oper);
            }
        }

        // temp
        private IGeneticOperator tempHybrid;
        public void removeHybrid(bool enabled)
        {
            if (enabled)
            {
                tempHybrid = operators[5];
                operators[5] = new NothingToDo();
            }
            else
            {
                if(tempHybrid != null)
                {
                    operators[5] = tempHybrid;
                }
            }
        }

        public int getGenerations()
        {
            return generations;
        }

        public string getPopuString()
        {
            StringBuilder sb = new StringBuilder();
            foreach(var v in population)
            {
                sb.Append(v.getGenotype().ToString());
            }

            sb.Append("\r\n");
            return sb.ToString();
        }
    }
}
