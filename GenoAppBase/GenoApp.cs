﻿using GenoAppBase.datalayer;
using GenoAppBase.dataobject;
using GenoAppBase.functions;
using GenoAppBase.genetic;
using GenoAppBase.genetic.operators.impl;
using GenoAppBase.util;
using System;
using System.IO;
using System.Threading;

namespace GenoAppBase
{
    public class GenoApp
    {
        private ExperimentInfo infoexp = new ExperimentInfo();
        private GeneticAlgoritm ga;
        public static string path = Environment.CurrentDirectory + "\\..\\..\\..\\results\\";

        public GenoApp()
        {

        }

        public GenoApp(ExperimentInfo infoexp)
        {
            this.infoexp = infoexp;
        }

        public ExperimentInfo getExperimentInfo()
        {
            return infoexp;
        }

        public GenoApp(string mutationRate, string crossoverRate, string populationSize, string countStopper, string accuracy,
            bool useHybrid, EFunction function, string functionSize)
        {
            setMutationRate(mutationRate);
            setCrossoverRate(crossoverRate);
            setPopulationSize(populationSize);
            setCountStopper(countStopper);
            setAccuracy(accuracy);
            hybridEnabled(useHybrid);
          //  setFunctionSize(functionSize);  // always set function size first, after the function
            setFunction(function, functionSize);
        }

        public void setHybridRate(string rate)
        {
            infoexp.HybridRate = parseInt(rate);
        }

        public void setTimeStopper(string timeStopper)
        {
            infoexp.TimeStopper = parseInt(timeStopper);
        }

        public void setStop(bool stop)
        {
            if(ga != null)
            {
                ga.setStop(stop);
            }
        }

        public void setFunctionSize(string size)
        {
            infoexp.FunctionSize = parseInt(size);
        }

        public void setMutationRate(string rate)
        {
            infoexp.MutationRate = parseInt(rate);
        }

        public void setCrossoverRate(string rate)
        {
            infoexp.CrossoverRate = parseInt(rate);
        }

        public void setPopulationSize(string size)
        {
            infoexp.PopulationSize = parseInt(size);
        }

        public void setCountStopper(string count)
        {
            infoexp.CountStopper = parseInt(count);
        }

        public void setAccuracy(string accuracy)
        {
            infoexp.Accuracy = double.Parse(accuracy);
        }

        public void hybridEnabled(bool enable)
        {
            infoexp.IsHybrid = enable;
        }

        public void setFunction(EFunction function, string size)
        {
            setFunctionSize(size);
            infoexp.FunctionName = function;
        }

        public void runInThread()
        {
            new Thread(new ThreadStart(run)).Start();
        }

        public void removeHybrid(bool enable)
        {
            if (ga != null)
            {
                ga.removeHybrid(enable);
            }
        }

        public void run()
        {
            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException("Directory \"results\" not found.");
            }

            string exp_file = path + "experiments.txt";
            if (!File.Exists(exp_file))
            {
                try
                {
                    File.Create(exp_file);
                }
                catch (Exception e)
                {
                    throw new Exception("Failed to create a new file " + exp_file + ": " + e.Message);
                }
            }

            DateTime startTime = DateTime.Now;
            string fileName = "genoexp_" + startTime.ToString("ddMMyy_HHmmss") + ".txt";
            string fileNameBeforeStrongin = "genoexp_" + startTime.ToString("ddMMyy_HHmmss") + "_before_strongin.txt";
            string fileNameAfterStrongin = "genoexp_" + startTime.ToString("ddMMyy_HHmmss") + "_after_strongin.txt";
            string fileNameArgs = "genoexp_" + startTime.ToString("ddMMyy_HHmmss") + "_args.txt";
            string filePath = path + fileName;

            infoexp.Id = fileName;
            GenoDaoFactory.getInstance().getDao(DataType.FILE).saveExperimentInfo(infoexp);

            ga = new GeneticAlgoritm((int)infoexp.CountStopper, (int) infoexp.TimeStopper, (int)infoexp.PopulationSize, startTime);
            ga.setOperators(new InitRandomPopulation((int)infoexp.PopulationSize));


            ga.setOperators(new WriterLog(filePath, startTime, ga));
            ga.setOperators(new ArithmeticCrossover((int)infoexp.CrossoverRate));
            ga.setOperators(new Mutation((int)infoexp.MutationRate));
      //      ga.setOperators(new WriterLogArg(GenoApp.path + "args.txt"));
            if ((bool)infoexp.IsHybrid)
            {
                ga.setOperators(new LocalAdaptation((int)infoexp.Accuracy, (int)infoexp.HybridRate));
      //          ga.setOperators(new WriterLogArg(GenoApp.path + "args.txt"));
            }
            ga.setOperators(new Selector((int)infoexp.PopulationSize));

            double[] min = ga.FindMinArg(FunctionFactory.getInstance().get(infoexp.FunctionName, (int)infoexp.FunctionSize));
        }

        private int parseInt(string s)
        {
            try
            {
                return int.Parse(s);
            }
            catch(FormatException e)
            {
                throw new FormatException("\"" + s + "\" is not value input: " + e.Message);
            }
            
        }

        public override string ToString()
        {
            return infoexp.FunctionName + " : " + ((bool)infoexp.IsHybrid ? "hybrid" : "classic");
        }
    }
}
