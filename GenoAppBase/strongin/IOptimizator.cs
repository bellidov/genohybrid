﻿using GenoAppBase.functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.strongin
{
    interface IOptimizator
    {
        double argMin(IFunction f);
        double getMinFunction();
    }
}
