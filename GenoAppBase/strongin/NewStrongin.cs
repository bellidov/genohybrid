﻿using GenoAppBase.functions;
using GenoAppBase.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.strongin
{
    class NewStrongin : IOptimizator
    {
        private double a, b, currentL, error;
        private double Xmin, Xmax;
        private double Zmin, Zmax;
        private double currentXr, currentZr; // currentZr = f.getValue(arg(currentXr));
        private IFunction f;
        private double[] toFix;
        private int index;
        public static bool stop = false;

        public NewStrongin(double a, double b, double[] toFix, int index, double error)
        {
            this.a = a; //min value for toFix[index]
            this.b = b; //max value for toFix[index]
            this.error = error;
            this.toFix = toFix;
            this.index = index;

            Xmin = a;
            Xmax = b;
        }

        public double argMin(IFunction f)
        {
            this.f = f;
            Zmin = f.getValue(arg(Xmin));
            Zmax = f.getValue(arg(Xmax));

            currentL = getL(Xmin, Xmax, Zmin, Zmax);
            currentXr = getXr(Xmin, Xmax, Zmin, Zmax, currentL); // Xmin < Xr < Xmax
            currentZr = f.getValue(arg(currentXr));

            while (!stop)
            {
                double tempL1 = getL(Xmin, currentXr, Zmin, currentZr);
                double tempR1 = getR(Xmin, currentXr, Zmin, currentZr, tempL1);
                double tempXr1 = getXr(Xmin, currentXr, Zmin, currentZr, tempL1);

                double tempL2 = getL(currentXr, Xmax, currentZr, Zmax);
                double tempR2 = getR(currentXr, Xmax, currentZr, Zmax, tempL2);
                double tempXr2 = getXr(currentXr, Xmax, currentZr, Zmax, tempL2);

                if(tempR1 >= tempR2)
                {
                    Xmax = currentXr;
                    Zmax = currentZr;
                    currentXr = tempXr1;
                }
                else
                {
                    Xmin = currentXr;
                    Zmin = currentZr;
                    currentXr = tempXr2;
                }

                if (Math.Abs(currentXr - Xmin) < error)
                {
                    break;
                }

                currentZr = f.getValue(arg(currentXr));
            }
            return currentXr;
        }

        public double getMinFunction()
        {
            return currentZr;
        }


        private double[] arg(double value)
        {
            toFix[index] = value; // poluchim argument s izmenennim elementom
            return toFix;
        }

        // gets characteristic for interval
        private double getR(double minX, double maxX, double minF, double maxF, double L)
        {
            double R = (maxX - minX) * L + (maxF - minF) * (maxF - minF) / ((maxX - minX) * L) - 2 * (maxF + minF);
            return R;
        }

        // gets min arg for charact.
        private double getXr(double minX, double maxX, double minF, double maxF, double L)
        {
            double Xr = (maxX + minX) / 2 - (maxF - minF) / (2 * L);
            return Xr;
        }

        // gets L for interval
        private double getL(double minX, double maxX, double minF, double maxF)
        {
            double temp = Math.Abs((maxF - minF) / (maxX - minX));
            double L;

            if (temp == 0)
            {
                L = 1;
            }
            else
            {
                L = temp * 1.5;
            }

            return L;
        }
    }
}
