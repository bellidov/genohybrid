﻿using GenoAppBase.functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.strongin
{
    class Interval
    {
        private double min;
        private double max;
        private double fmin;
        private double fmax;
        private double R;
        private double X;    // last found x, i.e. X[k+1]
        private double L;    // lipshits parameter
        private IFunction f;

        public Interval(double min, double max, IFunction f)
        {
            this.f = f;
            this.min = min;
            this.max = max;
            double f1 = f.getValue(min);
            double f2 = f.getValue(max);
        }

        public Interval getNewInterval(double min)
        {
            Interval i = new Interval(min, max, f);
            max = min;
            L = getL();
            return i;
        }

        private double getL()
        {
            return Math.Abs((fmin - fmax) / (min - max));
        }
    }
}
