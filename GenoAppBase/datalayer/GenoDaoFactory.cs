﻿using GenoAppBase.datalayer.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.datalayer
{
    public class GenoDaoFactory
    {
        private static GenoDaoFactory instance = new GenoDaoFactory();
        private IGenoExpDAO dao;

        private GenoDaoFactory()
        {

        }

        public static GenoDaoFactory getInstance()
        {
            return instance;
        }

        public IGenoExpDAO getDao(DataType type)
        {
            switch (type)
            {
                case DataType.FILE:
                    dao = new GenoExpFileBasedDAO();
                    break;
            }
            return dao;
        }
    }
}
