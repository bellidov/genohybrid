﻿using GenoAppBase.dataobject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.datalayer
{
    public interface IGenoExpDAO
    {
        List<ExperimentInfo> getExperiments();
        ExperimentInfo getExperimentByName(string name);
        ExperimentData getExperimentData(string name);
        void saveExperimentInfo(ExperimentInfo info);
        void saveDefaultExperimentInfo(ExperimentInfo info);
        ExperimentInfo getDefaultExperimentInfo();
    }
}
