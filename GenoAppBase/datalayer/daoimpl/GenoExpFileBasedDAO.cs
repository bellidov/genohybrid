﻿using GenoAppBase.dataobject;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GenoAppBase.datalayer.impl
{
    public class GenoExpFileBasedDAO : IGenoExpDAO
    {
        // public static readonly string resultsPath = "D:\\Programacion\\results\\";
        public static readonly string resultsPath = Environment.CurrentDirectory + "\\..\\..\\..\\results\\";
        public static readonly string filePath = resultsPath + "experiments.txt";
        private static readonly string defaultSettingsPath = "defaultSettings.txt";

        public List<ExperimentInfo> getExperiments()
        {
            List<ExperimentInfo> experiments = new List<ExperimentInfo>();
            string[] lines = readAllLines(filePath);

            foreach (var line in lines)
            {
                ExperimentInfo info = JsonConvert.DeserializeObject<ExperimentInfo>(line);
                experiments.Add(info);
            }

            return experiments;
        }

        public ExperimentInfo getExperimentByName(string name)
        {
            ExperimentInfo info = getExperiments().First(i => i.Id == name);

            return info;
        }

        public ExperimentData getExperimentData(string name)
        {
            string dataPath = resultsPath + name;
            List<double> x = new List<double>();    //time
            List<double> y = new List<double>();    //fitness
            List<double> z = new List<double>();    //iterations
            ExperimentData data = null;

            try {
                string[] lines = readAllLines(dataPath);

                foreach (var line in lines)
                {
                    string[] datax = line.Split(' ');
                    x.Add(Double.Parse(datax[2]));
                    y.Add(Double.Parse(datax[0]));
                    z.Add(Int32.Parse(datax[4]));
                }

                data = new ExperimentData(x.ToArray(), y.ToArray(), z.ToArray());
            }
            catch(Exception e)
            {
                    throw new Exception("Data file " + dataPath + " not found in the directory " + resultsPath + "or failed to read it.");
            //    data = null;
                
            }

            return data;
        }

        public void saveExperimentInfo(ExperimentInfo info)
        {
            using (StreamWriter writer = new StreamWriter(filePath, true, Encoding.Default))
            {
                writer.WriteLine(info);
            }
        }

        private string[] readAllLines(string path)
        {
            string[] lines = new string[10];
            if (File.Exists(path))
            {
                lines = File.ReadAllLines(path);
            }
            else
            {
                util.Log.info("the file " + path + "was not found.");
                throw new FileNotFoundException("the file " + path + "was not found or the path is invalid.");
            }
                
            return lines;
        }

        public void saveDefaultExperimentInfo(ExperimentInfo info)
        {
            using (StreamWriter writer = new StreamWriter(defaultSettingsPath, false, Encoding.Default))
            {
                writer.WriteLine(info);
            }
        }

        public ExperimentInfo getDefaultExperimentInfo()
        {
            ExperimentInfo info = null;
            if (File.Exists(defaultSettingsPath))
            {
                string[] lines = readAllLines(defaultSettingsPath);
                if(lines.Length == 1)
                {
                    info = JsonConvert.DeserializeObject<ExperimentInfo>(lines[0]);
                }  
            }
            return info;
        }
    }
}
