﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.dataobject
{
    public class ExperimentData
    {
        public double[] X { get; }  //time
        public double[] Y { get; }  //fitness
        public double[] Z { get; }  //ga iterations
        public int Iterations { get; }
        public int Time { get; }    // in seconds

        /// <param name="x">This is the time line in ms.</param>
        /// <param name="y">This is the fitness value.</param>
        /// <param name="z">This is the number of ga iterations. </param>
        public ExperimentData(double[] x, double[] y, double[] z)
        {
            int dim = 0;
            if(x.Length == y.Length)
            {
                dim = x.Length;
            }
            else
            {
                throw new Exception("The number of time points and fitness points must be equals");
            }
            //time
            X = new double[dim];
            //fitness
            Y = new double[dim];
            //iterations
            Z = new double[dim];

            //save the time in seconds
            for(int i = 0; i < dim; i++)
            {
                X[i] = Math.Truncate(x[i]) / 1000;
                Y[i] = Math.Truncate(y[i]);
                Z[i] = z[i];
            }

            // getting the total spent time
            Time = (int)Math.Truncate(X[X.Length - 1] - X[0]);

            // getting the total number of iterations spent
            Iterations = (int)Z[Z.Length - 1];
        }

    }
}
