﻿using GenoAppBase.datalayer;
using GenoAppBase.functions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GenoAppBase.dataobject
{
    public class ExperimentInfo
    {
        //file name for experiment data points (convergence)
        public string Id { set; get; }
        //general algorithm settings
        public int? CountStopper { set; get; }
        public int? TimeStopper { set; get; }
        public int? ConvergenceInterval { set; get; }
        //genetic settings
        public int? MutationRate { set; get; }
        public int? CrossoverRate { set; get; }
        public int? PopulationSize { set; get; }
        //strongin hybrid settings
        public double? Accuracy { set; get; }
        public bool? IsHybrid { set; get; }
        public int? HybridRate { set; get; }
        //function settings
        [JsonConverter(typeof(StringEnumConverter))]
        public EFunction FunctionName { set; get; }
        public int? FunctionSize { set; get; }
        public double[] MinDomain { set; get; }
        public double[] MaxDomain { set; get; }
        [JsonIgnore]
        private ExperimentData data;
        /*we must use nullable types to verify that user setted value for each param where it is required.*/

        public ExperimentInfo()
        {

        }

        public ExperimentData getData()
        {
            if(data == null)
            {
                //here is not the most apropiatte place to choose the data provider type...
                data = GenoDaoFactory.getInstance().getDao(DataType.FILE).getExperimentData(Id);
            }

            return data;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
