﻿using GenoAppBase.genetic.genotypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.util
{
    class Util
    {
        public static string popuToString(List<IGenotype> popu)
        {
            string pop = null;
            foreach(var v in popu)
            {
                pop += v + "\n";
            }

            pop += "\n";

            return pop;
        }

        public static string doubleArrayToString(double[] array)
        {
            string val = null;

            foreach (double v in array)
            {
                val += v + "_";
            }

            return val;
        }
    }
}
