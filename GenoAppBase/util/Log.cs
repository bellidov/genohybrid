﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.util
{
    public class Log
    {
        private static string fileName = GenoApp.path + "logs.txt";
        public static void info(string text)
        {
            using (StreamWriter writer = new StreamWriter(fileName, true, Encoding.Default))
            {
                writer.WriteLine(text);
            }
        }

        public static void funLog(string text)
        {
            string fileName = GenoApp.path + "strongin.txt";
            using (StreamWriter writer = new StreamWriter(fileName, false, Encoding.Default))
            {
                writer.WriteLine(text);
            }
        }

        public static void stronginLog(string text)
        {
            string fileName = GenoApp.path + "strongin.txt";
            using (StreamWriter writer = new StreamWriter(fileName, false, Encoding.Default))
            {
                writer.WriteLine(text);
            }
        }

        public static void argsLog(string text)
        {
            string fileName = GenoApp.path + "args.txt";
            using (StreamWriter writer = new StreamWriter(fileName, true, Encoding.Default))
            {
                writer.WriteLine(text);
            }
        }
    }
}
