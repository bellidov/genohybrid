﻿using GenoAppBase.functions.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.functions
{
    class FunctionFactory
    {
        private static FunctionFactory instance = new FunctionFactory();
        private IFunction function;

        private FunctionFactory()
        {

        }

        public static FunctionFactory getInstance()
        {
            return instance;
        }

        public IFunction get(EFunction name, int size)
        {
            switch (name)
            {
                case EFunction.RASTRIGIN:
                    function = new Rastrigin(size);
                    break;
                case EFunction.SCHWEFEL:
                    function = new Schwefel(size);
                    break;
                case EFunction.LEVI:
                    function = new Levi(size);
                    break;
                case EFunction.EGGHOLDER:
                    function = new EggHolder(size);
                    break;
            }
            return function;
        }
    }
}
