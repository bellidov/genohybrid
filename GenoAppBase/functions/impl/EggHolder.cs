﻿using GenoAppBase.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.functions.impl
{
    class EggHolder : AFunction
    {
        private static readonly double defaultMinDomain = -512;
        private static readonly double defaultMaxDomain = 512;
        private static int count;

        public EggHolder(int dim) : base(dim, defaultMinDomain, defaultMaxDomain)
        {
        }

        public EggHolder(double[] minDomain, double[] maxDoamin) : base(minDomain, maxDoamin)
        {
        }

        public override double getValue(params double[] arg)
        {
   //         Log.funLog(count.ToString());
   //         count++;
            int n = arg.Length;
            double result = 0;
            
            for(int i = 0; i <= n - 2; i++)
            {
                result += - (arg[i + 1] + 47) * Math.Sin(Math.Sqrt(Math.Abs(arg[i + 1] + 0.5 * arg[i] + 47))) +
                        (-arg[i]) * Math.Sin(Math.Sqrt(Math.Abs(arg[i] - (arg[i+1] + 47)))); 
            }

            return result;
        }
    }
}
