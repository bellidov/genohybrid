﻿using System;

namespace GenoAppBase.functions
{
    public class Rastrigin : AFunction
    {
        private const double defaultMinDomain = -5.12;
        private const double defaultMaxDomain = 5.12;

        public Rastrigin(int dim) : base(dim, defaultMinDomain, defaultMaxDomain)
        {
        }

        public Rastrigin(double[] minDomain, double[] maxDoamin) : base(minDomain, maxDoamin)
        {
        }

        public override double getValue(params double[] x)
        {
            if (x.Length != dim) throw new Exception("razmeri ne sovpadayut");

            double value = 10 * dim;

            for (int i = 0; i < dim; i++)
            {
                value += Math.Pow(x[i], 2) - 10 * Math.Cos(2 * Math.PI * x[i]);
            }

            return value;
        }
    }
}
