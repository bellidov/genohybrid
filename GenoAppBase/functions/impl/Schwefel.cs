﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.functions
{
    public class Schwefel : AFunction
    {
        private const double defaultMinDomain = -500;
        private const double defaultMaxDomain = 500;

        public Schwefel(int dim) : base(dim, defaultMinDomain, defaultMaxDomain)
        {
        }

        public Schwefel(double[] minDomain, double[] maxDoamin) : base(minDomain, maxDoamin)
        {
        }

        public override double getValue(params double[] x)
        {
            if (x.Length != dim) throw new Exception("razmeri ne sovpadayut");

            double result = 418.9829 * dim;

            for (int i = 0; i < dim; i++)
            {
                result -= x[i] * Math.Sin(Math.Sqrt(Math.Abs(x[i])));
            }

            return result;
        }
    }
}
