﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.functions
{
    public abstract class AFunction : IFunction
    {
        protected int dim = 0;
        private Domain domain;

        public AFunction()
        {

        }

        public AFunction(int dim, double defaultMinDomain, double defaultMaxDomain)
        {
            this.dim = dim;
            double[] minDomain = new double[dim];
            double[] maxDomain = new double[dim];

            for (int i = 0; i < dim; i++)
            {
                minDomain[i] = defaultMinDomain;
                maxDomain[i] = defaultMaxDomain;
            }

            domain = new Domain(minDomain, maxDomain);
        }

        public AFunction(double[] minDomain, double[] maxDoamin)
        {
            domain = new Domain(minDomain, maxDoamin);
            dim = domain.getDim();
        }

        public int getDim()
        {
            return dim;
        }

        public Domain getDomain()
        {
            return domain;
        }

        public abstract double getValue(params double[] arg);
    }
}
