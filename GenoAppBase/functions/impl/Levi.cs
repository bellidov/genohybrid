﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.functions.impl
{
    class Levi : AFunction
    {
        private static readonly double defaultMinDomain = -10;
        private static readonly double defaultMaxDomain = 10;

        public Levi(int dim) : base(dim, defaultMinDomain, defaultMaxDomain)
        {
        }

        public Levi(double[] minDomain, double[] maxDoamin) : base(minDomain, maxDoamin)
        {
        }

        // sin^2(3*pi*x0) + suma(0 : n-2):(xi-1)^2*(1+sin^2(3*pi*x(i+1))) + (x(n-1)-1)^2*(1+sin^2(2*pi*x(n-1)))

        public override double getValue(params double[] arg)
        {
            int n = arg.Length;

            double result = Math.Pow(Math.Sin(3*Math.PI*arg[0]), 2);


            for (int i = 0; i <= n - 2; i++)
            {
                result += Math.Pow(arg[i] - 1, 2) * (1 + Math.Pow(Math.Sin(3 * Math.PI * arg[i + 1]), 2)) + Math.Pow(arg[n - 1] - 1, 2) * (1 + Math.Pow(Math.Sin(2*Math.PI*arg[n-1]), 2));
            }

            return result;
        }
    }
}
