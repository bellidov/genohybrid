﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.functions
{
    public class Domain
    {
        public double[] minDomain;
        private double[] maxDomain;
        private int dim;

        public Domain(double[] minDomain, double[] maxDomain)
        {
            if(minDomain.Length != maxDomain.Length)
            {
                throw new IndexOutOfRangeException("minDomain and maxDomain length must be equals.");
            }

            this.minDomain = (double[])minDomain.Clone();
            this.maxDomain = (double[])maxDomain.Clone();
        }

        public int getDim()
        {
            return dim;
        }

        public double[] getMin()
        {
            return minDomain;
        }

        public double[] getMax()
        {
            return maxDomain;
        }
    }
}
