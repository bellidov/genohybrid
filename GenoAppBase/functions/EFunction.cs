﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoAppBase.functions
{
    public enum EFunction
    {
        RASTRIGIN,
        SCHWEFEL,
        LEVI,
        EGGHOLDER
    }
}
