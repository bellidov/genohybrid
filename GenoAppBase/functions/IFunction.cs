﻿namespace GenoAppBase.functions
{
    // in general, this is a multidimentional function
    public interface IFunction
    {
        double getValue(params double[] arg);
        int getDim();
        Domain getDomain();
    }
}
